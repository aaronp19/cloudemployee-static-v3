

$(document).ready(function () {
    
    if ($(window).width() >= 992){
        if($('body').hasClass("pricing-comparison")) {
            var heropriceletter = $(".ce-resources-pricing-h1").text().length; 
            if (heropriceletter > 20) {
               $(".ce-resources-pricing-h1").css({ "font-size": "70px" });
            }
        }
       
       var heroletters = $(".ce-hero-section-wrapper-title h1").text().length;
       if (heroletters > 15) {
           $(".ce-hero-section-wrapper-title h1").css({ "font-size": "50px" });
       }
       else if (heroletters == 15 && heroletters > 10) {
           $(".ce-hero-section-wrapper-title h1").css({ "font-size": "60px" });
       }
       else {
           $(".ce-hero-section-wrapper-title h1").css({ "font-size": "80px" });
       }
    }
    if ($(window).width() <= 991){
       var heroletters = $(".ce-hero-section-wrapper-title h1").text().length;
       if (heroletters > 15) {
           $(".ce-hero-section-wrapper-title h1").css({ "font-size": "35px" });
       }
       else {
           $(".ce-hero-section-wrapper-title h1").css({ "font-size": "45px" });
       }
    }
    var isSlideUp = false;
    $('.subscribe-modal').modal('show');
    $('.subscriber').click(function() {
        $('.ce-blog-fixed-email-subscription').fadeOut();
    });

    $('.subscriber').click(function() {
        isSlideUp = true;
    });

    $(document).scroll(function() {
        var y = $(this).scrollTop();
        if (!isSlideUp) {
            if (y > 500) {
                //$('.ce-blog-fixed-email-subscription').fadeIn();
                $('.ce-blog-fixed-email-subscription').slideDown(600)
            } else {
                //$('.ce-blog-fixed-email-subscription').fadeOut();   
                $('.ce-blog-fixed-email-subscription').slideUp(600);    
            }
        }

    });
                  
    $(".modal").on("hidden.bs.modal", function () {
        if ($(this).find('iframe').length > 0) {
            $(this).find('iframe').each(function () {
                $(this).removeAttr('src');
            });
        }
    });
    $('.modal').on('shown.bs.modal', function () {
        if ($(this).find('iframe').length > 0) {
            $(this).find('iframe').each(function () {
                $(this).attr({ 'src': $(this).data('src') });
            });
        }
    })

    var body = $("body");


    $('.news-list .news-item .news-thumbnail:not(.--js-loaded), div.lazy-load:not(.--js-loaded)').each(function () {
        if (!$(this).hasClass('modal-img')) {
            var t = $(this).offset().top;
            var h = $(window).scrollTop() + $(window).height();
            if (t < h) {
                $(this).css({ 'background-image': 'url(' + $(this).data('src') + ')' });
                $(this).removeAttr('data-src');
            }
        }
    });
    $('img.lazy-load:not(.--js-loaded)').each(function () {
        if (!$(this).hasClass('modal-img')) {
            var t = $(this).offset().top;
            var h = $(window).scrollTop() + $(window).height();
            if (t < h) {
                $(this).attr({ 'src': $(this).data('src') });
                $(this).removeAttr('data-src');
            }
        }
    });

    $('.navbar-toggle').click(function () {
        var scroll = $(window).scrollTop();
        var width = $(window).width();
        if (width < 992) {
            if (scroll >= 10) {
                if (!body.hasClass('scrolled')) {
                    body.toggleClass("scrolled");
                }
            }
            else {
                body.toggleClass("scrolled");
            }
        }
    });

    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        var width = $(window).width();
        if ($('body').hasClass('news') !== true) {

            if (scroll >= 10) {
                body.addClass("scrolled");
            } else {
                if (width < 992) {
                    if (!$('#ce-navbar').hasClass('in')) {
                        body.removeClass("scrolled");
                    }
                }
                else {
                    body.removeClass("scrolled");
                }
            }
        }

        $('.news-item .news-thumbnail:not(.--js-loaded), div.lazy-load:not(.--js-loaded)').each(function () {
            var t = $(this).offset().top;
            var h = $(window).scrollTop() + $(window).height();
            if (t < h) {
                $(this).css({ 'background-image': 'url(' + $(this).data('src') + ')' });
                $(this).removeAttr('data-src');
            }
        });

        $('img.lazy-load:not(.--js-loaded)').each(function () {
            var t = $(this).offset().top;
            var h = $(window).scrollTop() + $(window).height();
            if (t < h) {
                $(this).attr({ 'src': $(this).data('src') });
                $(this).removeAttr('data-src');
            }
        });

        if (
            $('.news-list').length > 0
            && scroll >= $(document).height() - $('footer').height() - $(window).height()
            && $('.news-list').hasClass('--js-shown-all') !== true
        ) {

            if ($('.news-list').hasClass('loading') !== true) {
                $('.news-list').addClass('loading');

                var url = window.location.href;
                url = url.replace(location.protocol + "//" + location.host, '').toLowerCase().split('/');

                var queryString = '';
                if (url[2] == 'category') {
                    queryString += "&category=" + url[3];
                } else if (url[2] == 'tag') {
                    queryString += "&tag=" + url[3];
                } else {
                    queryString += "&year=" + url[2];
                    if (url[3] !== null) {
                        queryString += "&month=" + url[3];
                    }
                }

                var month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                var html = '<a href="[Url]" target="_blank" class="col-md-6 col-xs-12 news-item"><div class="overlay-title"><span class="gotham-bold">[Title]<small>[Author]</small></span></div><div class="news-thumbnail --js-loaded" style="background-image: url([BannerImage]);"></div><div class="date-posted"><span class="month">[ReleaseMonth]</span>[ReleaseDay]</div></a>';
                $.ajax({
                    url: location.protocol + "//" + location.host + '/umbraco/api/BlogData/Get',
                    type: 'pOsT', /* this works like a charm */
                    contentType: "application/x-www-form-urlencoded",
                    data: { "": "skip=" + $(".news-item").length + queryString },
                    success: function (data) {
                        data = JSON.parse(data);

                        if (data.Blogs.length > 0) {
                            if (data.ResponseCode !== null && data.ResponseCode == '1') {
                                var str = '';

                                for (var i = 0; i < data.Blogs.length; i++) {
                                    var blog = data.Blogs[i];
                                    str = str + html.split('[Url]').join(blog.Url)
                                                    .split('[Author]').join("by " + blog.Author)
                                                    .split('[Title]').join(blog.Title)
                                                    .split('[BannerImage]').join(blog.BannerImage)
                                                    .split('[ReleaseDay]').join(blog.ReleaseDay)
                                                    .split('[ReleaseMonth]').join(month[parseInt(blog.ReleaseMonth) - 1]);
                                }
                                $('#news-list .row').append(str);

                            }
                        } else {
                            $('#news-list').addClass('--js-shown-all');
                        }
                        $('#news-list').removeClass('loading');
                    },
                });
            }
        } else if ($('.ce-staffs').length > 0) {
            var tStr = '<div class="col-md-3 col-sm-4 col-xs-6 ce-item "><div data-toggle="modal" data-target="#[Id]" class="staff-thumb lazy-load --js-loaded" style="background-image: url([Image])"><div class="staff-info"><h2 class="name">[Name]</h2>[Position]</div></div></div>';
            var mStr = '<div class="modal fade staff-details in" id="[Id]" tabindex="-1" role="dialog" aria-labelledby="[Id]-Label"><div class="vertical-alignment-helper"><div class="modal-dialog vertical-align-center"><div class="modal-content"><div class="modal-body"><span class="close" data-dismiss="modal" aria-label="Close">&times;</span><div class="thumbnail col-md-3 col-xs-3"><img class="lazy-load --js-loaded" alt="[Name]" src="[Image]"><div class="name">[Name]</div>[Position][Experience][Country]</div><div class="details col-md-9 col-xs-9">[Description]</div></div></div></div></div></div></div>';


            if ($('.ce-staffs').hasClass('loading') !== true
                && scroll >= $(document).height() - $('footer').height() - $(window).height()
                && $('.ce-staffs').hasClass('--js-shown-all') !== true
            ) {
                $('.ce-staffs').addClass('loading');

                var staffs = '';
                var modals = '';
                $.ajax({
                    url: location.protocol + "//" + location.host + '/umbraco/api/TeamData/Get',
                    type: 'pOsT', /* this works like a charm */
                    contentType: "application/x-www-form-urlencoded",
                    data: { "": "skip=" + $(".ce-item").length },
                    success: function (data) {
                        data = JSON.parse(data);


                        if (data.Members.length > 0) {
                            if (data.ResponseCode !== null && data.ResponseCode == '1') {
                                for (var i = 0; i < data.Members.length; i++) {
                                    var staff = data.Members[i];

                                    staffs += tStr.split('[Id]').join(staff.Id)
                                                .split('[Image]').join(staff.Image)
                                                .split('[Name]').join(staff.Name)
                                                .split('[Position]').join('<p class="position">' + staff.Position + '</p>');


                                    if (staff.ShowPopup === true) {
                                        var temp = mStr.split('[Id]').join(staff.Id)
                                                    .split('[Image]').join(staff.Image)
                                                    .split('[Name]').join(staff.Name)
                                                    .split('[Description]').join(staff.Description)
                                                    .split('[Position]').join('<span class="position">' + staff.Position + '</span>');

                                        if (staff.Experience != "") {
                                            temp = temp.split('[Experience]').join('<span class="years-xp">' + staff.Experience + '</span>');
                                        } else {
                                            temp = temp.split('[Experience]').join('');
                                        }
                                        if (staff.Country != "") {
                                            temp = temp.split('[Country]').join('<span class="country">' + staff.Country + '</span>');
                                        } else {
                                            temp = temp.split('[Country]').join('');
                                        }
                                        modals += temp;
                                    }

                                }
                                $('.ce-staffs .row').append(staffs);
                                $('body').append(modals);
                            }
                        } else {
                            $('.ce-staffs').addClass('--js-shown-all');
                        }

                        $('.ce-staffs').removeClass('loading');
                    },
                });
            }
        }
    });


    var loopTestimonial = false;

    if ($('.slider ul li').length > 1) {
        loopTestimonial = true;
    }

    var testimonials = new Swiper('.slider', {
        slidesPerView: 1,
        loop: loopTestimonial,
        nextButton: '.slick-next',
        prevButton: '.slick-prev'
    });

    var loopTestimonial1 = false;

    if ($('.slider-hiw ul li').length > 1) {
        loopTestimonial1 = true;
    }

    var testimonials1 = new Swiper('.slider-hiw', {
        slidesPerView: 2,
        loop: loopTestimonial1,
        nextButton: '.slick-next',
        prevButton: '.slick-prev'
    });



    /* Footer images start */
    var footerImages3 = new Swiper('.footer-flip-slider-3', {
        slidesPerView: 1,
        grabCursor: true,
        effect: 'flip',
        loop: true

    });
    var footerImages2 = new Swiper('.footer-flip-slider-2', {
        slidesPerView: 1,
        grabCursor: true,
        effect: 'flip',
        loop: true,
        onSlideChangeEnd: function (swiper) {

            setTimeout(function () {
                footerImages3.slideNext();
            }, 500); //delays next call for .5 seconds
        }
    });


    var footerImages = new Swiper('.footer-flip-slider', {
        slidesPerView: 1,
        grabCursor: true,
        effect: 'flip',
        loop: true,
        autoplay: 3000,
        autoplayDisableOnInteraction: false,
        onSlideChangeEnd: function (swiper) {

            setTimeout(function () {
                footerImages2.slideNext();
            }, 500); //delays next call for .5 seconds
        }
    });
    /* Footer images end */



    $(window).scroll(function (event) {
        $(".full-width").each(function () {
            if ($(this).hasClass('.full-beyond-offshoring')) {
                if ($(this).position().top <= $(document).scrollTop() + (($(window).height() / 5) * 1)) {
                    $(this).addClass('screen-active');
                }
            } else {
                if ($(this).position().top <= $(document).scrollTop() + (($(window).height() / 5) * 3)) {
                    $(this).addClass('screen-active');
                }
            }
        });


        $('.faqs-accordion > .ac-item').each(function () {
            if ($(this).position().top <= $(document).scrollTop()) {
                $('.faq-navigation').find('li').removeClass('active');

                $('.faq-navigation').find('a[data-target=#' + $(this).attr('id') + ']').closest('li').addClass('active');

                console.log($(this).attr('id'));
            }
        });

    });


    $(document).on('click', '.faq-navigation a', function () {
        $(this).closest('ul').find('li').removeClass('active');
        $(this).closest('li').addClass('active');

        var temp = $('.faq-accordion').find($(this).data('target')).offset().top;
        $('html, body').animate({
            scrollTop: temp - 100
        }, 500);
        return false;
    });


    $('.btn-more,.get-started').click(function () {
        var e = $(this).closest('.full-width').next('.full-width');
        var temp = 0;

        if ($('body').hasClass('homepage')) {
            temp = e.offset().top - $('.navbar').height();

        } else {

            if ($(this).hasClass('get-started')) {
                e = e.offset().top;
                temp = e + 100;
            } else if (e.find('h2').length > 0) {
                temp = e.find('h2').offset().top;
            } else {
                e = e.next(".full-width");
                temp = e.find('h2').offset().top;
            }

            if ($(this).hasClass('.get-started')) {
                temp = temp + 400;
            }

            console.log(temp);
            temp = temp - 150;
        }


        $('html, body').animate({
            scrollTop: temp
        }, 500);
        return false;
    });

    $('.learn-more-button').click(function () {
        var e = $(this).closest('.full-width').next('.full-width');
        var temp = e.offset().top;

        // if($('body').hasClass('homepage') !== true){
        temp = temp - $('.navbar').height();
        // }

        $('html, body').animate({
            scrollTop: temp
        }, 500);
        return false;
    });
    
    
    $(".orange-submit-req").click(function () {
        var temp = $(".grow-team").offset().top;

        temp = temp - 50;
        $('html,body').animate({
            scrollTop: temp
        }, 500);
    });

    $(".ce-enquire-now").click(function () {
        var temp = $(".ce-ads-landing-contact").offset().top;

        temp = temp - 200;
        $('html,body').animate({
            scrollTop: temp
        }, 500);
    });



    $(document).on('click', '.office-item .overlay,.office-item .overlay span', function () {

        $(this).closest('.our-offices').find('.office-item').removeClass('active');

        $(this).closest('.office-item').addClass('active');

        // if ($(window).width() <= 991) {
        //     $('body').addClass('no-scroll');
        // }
        return false;

    });
    $(document).on('click', '.office-item .close', function () {
        $(this).closest('.office-item').removeClass('active');
        $('body').removeClass('no-scroll');

        return false;

    });


    if ($('body').hasClass('homepage') || $('body').hasClass('ourstory') || $('body').hasClass('howitworks')) {
        getVideoHeight(
            "/assets/vid/ce-video.mp4",
            function (w, h) {
                var conW = $('video').closest('.container').width();

                var per = (conW / w);



                $('.full-width:not(.island-video) .video-js').css({
                    width: (w * per) + 'px',
                    height: (h * per) + 'px'
                });

            }
        );
    }


    function videoMargin() {
        if ($('body').hasClass('homepage')) {

            var v = $('.island-video video').width();
            var w = $(window).width();


            var x = (v - w) / 2;


            $('.island-video video').css({ 'margin-left': '-' + x + 'px' })

        }
    }

    $(".doaccordion").accordion(
        {
            active: false,
            collapsible: true,
            icons: false,
            heightStyle: "content",


            beforeActivate: function (event, ui) {
                // The accordion believes a panel is being opened
                if (ui.newHeader[0]) {
                    var currHeader = ui.newHeader;
                    var currContent = currHeader.next('.ui-accordion-content');
                    // The accordion believes a panel is being closed
                } else {
                    var currHeader = ui.oldHeader;
                    var currContent = currHeader.next('.ui-accordion-content');
                }
                // Since we've changed the default behavior, this detects the actual status
                var isPanelSelected = currHeader.attr('aria-selected') == 'true';

                // Toggle the panel's header
                currHeader.toggleClass('ui-corner-all', isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top', !isPanelSelected).attr('aria-selected', ((!isPanelSelected).toString()));

                // Toggle the panel's icon
                currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e', isPanelSelected).toggleClass('ui-icon-triangle-1-s', !isPanelSelected);

                // Toggle the panel's content
                currContent.toggleClass('accordion-content-active', !isPanelSelected)
                if (isPanelSelected) { currContent.slideUp(); } else { currContent.slideDown(); }

                return false; // Cancel the default action

            }

        }
    );

    var numCols = 3;
    var windowWidth = $(window).width();
    if (windowWidth >= 501 && windowWidth <= 757) {
        numCols = 2;
    }
    if (windowWidth <= 501) {
        numCols = 1;
    }


    var t = $(".cols").width() / numCols;
    $('.cols .box').each(function () {
        $(this).css({ "max-width": t + "px" });
    });
    $('.cols').masonry({
        // options
        itemSelector: '.box',
        columnWidth: t
    });

    $('.btn-live-chat').click(function () {
        $('.olark-button-text').click();
        return false;
    });

    /*devhireprom*/
    var utmsource = getParameterByName("utm_source");
    var utmmedium = getParameterByName("utm_medium");
    var utmcampaign = getParameterByName("utm_campaign");
    var utmterm = getParameterByName("utm_term");
    var utmcontent = getParameterByName("utm_content");

    Cookies.set("utmsource", utmsource, { expires: 1 });
    Cookies.set("utmmedium", utmmedium, { expires: 1 });
    Cookies.set("utmcampaign", utmcampaign, { expires: 1 });
    Cookies.set("utmterm", utmterm, { expires: 1 });
    Cookies.set("utmcontent", utmcontent, { expires: 1 });

    $('#btn-hire-dev-form-desktop').click(function (e) {
        var validate = $('#hire-dev-form-desktop')[0].checkValidity();
        if (validate) {
            $('.utmsource').val(Cookies.get('utmsource'));
            $('.utmmedium').val(Cookies.get('utmmedium'));
            $('.utmcampaign').val(Cookies.get('utmcampaign'));
            $('.utmterm').val(Cookies.get('utmterm'));
            $('.utmcontent').val(Cookies.get('utmcontent'));
            $.post(location.protocol + "//" + location.host + '/umbraco/api/hiredev/submit', { "": $('#hire-dev-form-desktop').serialize() })
            .success(function (message) {
                var data = message.split("|");
                if (data[0] !== "0") {
                    if (data[2] !== "") {
                        window.location = data[2];
                    }
                    else {
                        $('.hire-dev-form-notif').show().text(data[1]);
                    }
                    $('#btn-hire-dev-form-desktop').val("Submit").removeAttr("disabled");
                    $('#btn-hire-dev-form-tabmob').val("Submit").removeAttr("disabled");
                }
                else {
                    $('.hire-dev-form-notif').show().text(data[1]);
                    $('#btn-hire-dev-form-desktop').val("Submit").removeAttr("disabled");
                    $('#btn-hire-dev-form-tabmob').val("Submit").removeAttr("disabled");
                }
            })
            .error(function (message) {
                $('.hire-dev-form-notif').show().text(message);
                $('#btn-hire-dev-form-desktop').val("Submit").removeAttr("disabled");
                $('#btn-hire-dev-form-tabmob').val("Submit").removeAttr("disabled");
            });

            $(this).attr('disabled', 'disabled');
            $(this).attr('value', 'Processing');
            e.preventDefault();
        }
    });

    $('#btn-hire-dev-form-tabmob').click(function (e) {
        var validate = $('#hire-dev-form-tabmob')[0].checkValidity();
        if (validate) {
            $('.utmsource').val(Cookies.get('utmsource'));
            $('.utmmedium').val(Cookies.get('utmmedium'));
            $('.utmcampaign').val(Cookies.get('utmcampaign'));
            $('.utmterm').val(Cookies.get('utmterm'));
            $('.utmcontent').val(Cookies.get('utmcontent'));
            $.post(location.protocol + "//" + location.host + '/umbraco/api/hiredev/submit', { "": $('#hire-dev-form-tabmob').serialize() })
            .success(function (message) {
                var data = message.split("|");
                if (data[0] !== "0") {
                    if (data[2] !== "") {
                        window.location = data[2];
                    }
                    else {
                        $('.hire-dev-form-notif').show().text(data[1]);
                    }
                    $('#btn-hire-dev-form-desktop').val("Submit").removeAttr("disabled");
                    $('#btn-hire-dev-form-tabmob').val("Submit").removeAttr("disabled");
                }
                else {
                    $('.hire-dev-form-notif').show().text(data[1]);
                    $('#btn-hire-dev-form-desktop').val("Submit").removeAttr("disabled");
                    $('#btn-hire-dev-form-tabmob').val("Submit").removeAttr("disabled");
                }
            })
            .error(function (message) {
                $('.hire-dev-form-notif').show().text(message);
                $('#btn-hire-dev-form-desktop').val("Submit").removeAttr("disabled");
                $('#btn-hire-dev-form-tabmob').val("Submit").removeAttr("disabled");
            });

            $(this).attr('disabled', 'disabled');
            $(this).attr('value', 'Processing');
            e.preventDefault();
        }
    });

    /*--devhireprom*/

    /*pricing comparison*/
    $('#btn-price-comparison-form-desktop').click(function (e) {
        var validate = $('#price-comparison-form-desktop')[0].checkValidity();
        if (validate) {
            $.post(location.protocol + "//" + location.host + '/umbraco/api/pricing/submit', { "": $('#price-comparison-form-desktop').serialize() })
            .success(function (message) {
                var data = message.split("|");
                if (data[0] !== "0") {
                    if (data[2] !== "") {
                        window.location = data[2];
                    }
                    else {
                        $('.price-comparison-form-notif').show().text(data[1]);
                    }
                    $('#btn-hire-dev-form-desktop').val("Submit").removeAttr("disabled");
                    $('#btn-hire-dev-form-tabmob').val("Submit").removeAttr("disabled");
                    $('#btn-price-comparison-form-desktop').text("Submit").removeAttr("disabled");
                }
                else {
                    $('.price-comparison-form-notif').show().text(data[1]);
                    $('#btn-hire-dev-form-desktop').val("Submit").removeAttr("disabled");
                    $('#btn-hire-dev-form-tabmob').val("Submit").removeAttr("disabled");
                    $('#btn-price-comparison-form-desktop').text("Submit").removeAttr("disabled");
                }
            })
            .error(function (message) {
                $('.price-comparison-form-notif').show().text(message);
                $('#btn-hire-dev-form-desktop').val("Submit").removeAttr("disabled");
                $('#btn-hire-dev-form-tabmob').val("Submit").removeAttr("disabled");
                $('#btn-price-comparison-form-desktop').text("Submit").removeAttr("disabled");
            });

            $(this).text("Processing").attr("disabled", "disabled");
            e.preventDefault();
        }
    });
    /*--pricing comparison*/
    /*pdf contact form*/
    $('#btn-pdf-contact-form-desktop').click(function (e) {
        var validate = $('#pdf-contact-form-desktop')[0].checkValidity();
        if (validate) {
            $.post(location.protocol + "//" + location.host + '/umbraco/api/pdfcontactform/submit', { "": $('#pdf-contact-form-desktop').serialize() })
            .success(function (message) {
                var data = message.split("|");
                if (data[0] !== "0") {
                    if (data[2] !== "") {
                        window.location = data[2];
                    }
                    else {
                        $('.pdf-contact-form-notif').show().text(data[1]);
                    }
                    $('#btn-pdf-contact-form-desktop').text("Submit").removeAttr("disabled");
                }
                else {
                    $('.pdf-contact-form-notif').show().text(data[1]);
                    $('#btn-pdf-contact-form-desktop').text("Submit").removeAttr("disabled");
                }
            })
            .error(function (message) {
                $('.pdf-contact-form-notif').show().text(message);
                $('#btn-pdf-contact-form-desktop').text("Submit").removeAttr("disabled");
            });

            $(this).text("Processing").attr("disabled", "disabled");
            e.preventDefault();
        }
    });
    /*--pdf contact form*/
    /*slick image slider blog section START*/
    $(".regular").slick({
        dots: true,
        arrows: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        responsive: [
        {
            breakpoint: 769,
            settings: {
                dots: true,
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                adaptiveHeight: true
            }
        }]
    });
    /*slick image slider blog section form END*/
});


function getVideoHeight(url, fnCallback) {

    var video = document.createElement("video");
    video.autoplay = true;
    video.oncanplay = function () {
        fnCallback(this.offsetWidth, this.offsetHeight);
        this.src = "about:blank";
    };

    document.body.appendChild(video);
    video.src = url;
    document.body.removeChild(video);

}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

$(document).resize(function () {
    $('.island-video video').css({ width: $(window).width() });
});
$('.island-video video').load(function () {
    // videoMargin();
});
$(window).load(function () {


    setTimeout(function () {
        $('.full-width.fade-on-load').addClass('screen-active');
    }, 500);
    $('.island-video .overlay').remove();
    // videoMargin();

    //scroll to faq on load start
    var q = getParameterByName('question') || getParameterByName('q'),
        scrollSpeed = 1000,
        offsetFromHeader = 100,
        isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);

    if (q !== null) {
        if ($('.scroll-view').hasClass(q)) {
            $(isChrome ? "body" : "html").stop().animate({ scrollTop: $('.' + q).offset().top - offsetFromHeader }, scrollSpeed, function () {
                $('.' + q).click();
            });
        }
    }
    //scroll to faq on load end

});


$(window).resize(function () {
    var scroll = $(window).scrollTop();
    var width = $(window).width();
    if (scroll < 10) {
        if (width >= 992) {
            $("body").removeClass("scrolled");
        }
        else {
            if ($('#ce-navbar').hasClass('in')) {
                $("body").addClass("scrolled");
            }
        }
    }

    if ($('body').hasClass('homepage') || $('body').hasClass('ourstory') || $('body').hasClass('howitworks')) {


        getVideoHeight(
            "/assets/vid/ce-video.mp4",
            function (w, h) {
                var conW = $('video').closest('.container').width();

                var per = (conW / w);



                $('.full-width:not(.island-video) .video-js').css({
                    width: (w * per) + 'px',
                    height: (h * per) + 'px'
                });

            }
        );
    }
});

$(window).resize(function () {
    var numCols = 3;
    var windowWidth = $(window).width();
    if (windowWidth >= 501 && windowWidth <= 757) {
        numCols = 2;
    }
    if (windowWidth <= 501) {
        numCols = 1;
    }


    var t = $(".cols").width() / numCols;
    $('.cols').masonry("destroy");
    $(".cols .box").each(function () {
        $(this).css({ "max-width": t + "px" });
    });
    $('.cols').masonry({
        // options
        itemSelector: '.box',
        columnWidth: t
    });
});


$(window).on('beforeunload', function () {
    //$(window).scrollTop(0);
});


if ($('body').hasClass('contactus-')) {

    var latLng = new google.maps.LatLng(51.5098172, -0.1466952);
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: latLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: true,
        draggable: true,
        styles: [
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#00b5ec"
            },
            {
                "visibility": "on"
            }
        ]
    }
        ]
    });

    var locations = [
        {
            office: 'UK Office',
            officeClass: 'uk-office',
            officeEmail: 'hello@cloudemployee.co.uk',
            officeTel: '+44 20 3287 2488',
            lat: 51.5098172,
            lng: -0.1466952
        },
        {
            office: 'Australian Office',
            officeClass: 'Australian Office',
            officeEmail: ' australia@cloudemployee.co.uk',
            officeTel: '+61 280 060774',
            lat: -33.8685431,
            lng: 151.193414
        },
        {
            office: 'Manila Office',
            officeClass: '6789-office',
            officeEmail: 'manila@cloudemployee.co.uk',
            officeTel: '',
            lat: 14.5576271,
            lng: 121.0200075
        },
        {
            office: 'Manila Office',
            officeClass: 'zeta-office',
            officeEmail: 'manila@cloudemployee.co.uk',
            officeTel: '',
            lat: 14.5574547,
            lng: 121.0150765
        },
        {
            office: 'Cebu Office',
            officeClass: 'cebu-office',
            officeEmail: 'manila@cloudemployee.co.uk',
            officeTel: '',
            lat: 10.329277,
            lng: 123.9069226
        }
    ];
    var bounds = new google.maps.LatLngBounds();
    for (i = 0; i < locations.length; i++) {

        var homeLatLng = new google.maps.LatLng(locations[i].lat, locations[i].lng);
        var marker = new MarkerWithLabel({
            position: homeLatLng,
            draggable: false,
            raiseOnDrag: true,
            map: map,
            labelContent: "<div class='map-label " + locations[i].officeClass + "' data-office='" + locations[i].office + "'><span><img src='/assets/img/map-pointer.png' />" + locations[i].office + "</div>",
            labelAnchor: new google.maps.Point(0, 0),
            labelClass: "labels ", // the CSS class for the label
            isClicked: false,
            icon: " ",
        });
        bounds.extend(homeLatLng);



        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                marker.set("labelClass", "labels selected ");
                showContactForm(locations[i].office, locations[i].officeEmail, locations[i].officeTel);
            }
        })(marker, i));


    }
    map.fitBounds(bounds);


}

function showContactForm(office, email, tel) {
    $('.contact-form-container').hide();
    $('.office-name').text(office);
    if (tel != '' && tel != undefined) {
        $('.office-tel').text(tel);
        $('.office-tel').closest('p').show();
    } else {
        $('.office-tel').closest('p').hide();
    }
    $('.contact-form-container .contact-form input[name=officeEmail]').val(email);
    $('.contact-form-container').show();
}

function hideContact() {
    $('.contact-form-container').hide();
}

function browseFiles() {
    $('input[name=cv-resume]').click();
    return false;
}

$('.file-upload button').click(function () {
    $('input[name=cv-resume]').click();
    return false;
});


$('#contact-form-main-submit').click(function (e) {
    var validate = $('#contact-form-main')[0].checkValidity();
    if (validate) {
        $.post(location.protocol + "//" + location.host + '/umbraco/api/contact/sendcontact', { "": $('#contact-form-main').serialize() })
        .success(function (message) {
            var data = message.split("|");
            if (data[0] !== "0") {
                //$(".contact-form-control").hide();
                window.location = data[2];
            }
            $('#contact-form-main-submit').text("Submit").removeAttr("disabled");
            //$('.contact-form-notif').show().text(data[1]);
        })
        .error(function (message) {
            $(".contact-form-control").show();
        });

        $('#contact-form-main-submit').text("Processing your request").attr("disabled", "disabled");
        e.preventDefault();
    }
});

$('#contact-form-nav-submit').click(function (e) {
    var validate = $('#contact-form-nav')[0].checkValidity();
    if (validate) {
        $.post(location.protocol + "//" + location.host + '/umbraco/api/contact/sendcontact', { "": $('#contact-form-nav').serialize() })
            .success(function (message) {
                var data = message.split("|");
                if (data[0] !== "0") {
                    //$(".contact-form-control").hide();
                    window.location = data[2];
                }
                $('#contact-form-nav-submit').text("Submit").removeAttr("disabled");
                //$('.contact-form-notif').show().text(data[1]);
            })
            .error(function (message) {
                $(".contact-form-control").show();
            });

        $('#contact-form-nav-submit').text("Processing your request").attr("disabled", "disabled");
        e.preventDefault();
    }
});

$('#contact-form-modal-submit').click(function (e) {
    var validate = $('#contact-form-modal')[0].checkValidity();
    if (validate) {
        $.post(location.protocol + "//" + location.host + '/umbraco/api/contact/sendcontact', { "": $('#contact-form-modal').serialize() })
        .success(function (message) {
            var data = message.split("|");
            if (data[0] !== "0") {
                //$(".contact-form-control").hide();
                window.location = data[2];
            }
            $('#contact-form-modal-submit').text("Submit").removeAttr("disabled");
            //$('.contact-form-notif').show().text(data[1]);
        })
        .error(function (message) {
            $(".contact-form-control").show();
        });

        $('#contact-form-modal-submit').text("Processing your request").attr("disabled", "disabled");
        e.preventDefault();
    }
});

$('#contact-form-footer-submit').click(function (e) {
    var validate = $('#contact-form-footer')[0].checkValidity();
    if (validate) {
        $.post(location.protocol + "//" + location.host + '/umbraco/api/contact/sendcontact', { "": $('#contact-form-footer').serialize() })
        .success(function (message) {
            var data = message.split("|");
            if (data[0] !== "0") {
                //$(".contact-form-control").hide();
                window.location = data[2];
            }
            $('#contact-form-footer-submit').text("Submit").removeAttr("disabled");
            //$('.contact-form-notif').show().text(data[1]);
        })
        .error(function (message) {
            $(".contact-form-control").show();
        });

        $('#contact-form-footer-submit').text("Processing your request").attr("disabled", "disabled");
        e.preventDefault();
    }
});

$('#btn-subscribe').click(function (e) {
    var validate = $('#header-subscribe-form')[0].checkValidity();
    if (validate) {
        $.post(location.protocol + "//" + location.host + '/umbraco/api/signup/add', { "": $('#header-subscribe-form').serialize() })
           .success(function (message) {
               var data = message.split("|");
               $('#subscription-text').text(data[1]);
               $('#subscriptionmodal').modal('show');
               $('#btn-subscribe').text("Subscribe").removeAttr("disabled");
           })
           .error(function (message) {
               var data = message.split("|");
               $('#subscription-text').text(data[1]);
               $('#subscriptionmodal').modal('show');
               $('#btn-subscribe').text("Subscribe").removeAttr("disabled");
           });
        $('#btn-subscribe').text("Processing your request").attr("disabled", "disabled");
        e.preventDefault();
    }
});

$('.btn-subscribe').click(function (e) {
    var validate = $('#subscribe-form')[0].checkValidity();
    if (validate) {
        $.post(location.protocol + "//" + location.host + '/umbraco/api/signup/add', { "": $('#subscribe-form').serialize() })
           .success(function (message) {
               var data = message.split("|");
               $('#subscription-text').text(data[1]);
               $('#subscriptionmodal').modal('show');
               $('.btn-subscribe').text("Subscribe").removeAttr("disabled");
           })
           .error(function (message) {
               var data = message.split("|");
               $('#subscription-text').text(data[1]);
               $('#subscriptionmodal').modal('show');
               $('.btn-subscribe').text("Subscribe").removeAttr("disabled");
           });
        $('.btn-subscribe').text("Processing your request").attr("disabled", "disabled");
        e.preventDefault();
    }
});

$('#filter-archives').change(function () {
    window.location = $(this).find(":selected").val();
});

$('#cv-upload-btn').click(function () {
    $('#cv-upload').click();
    return false;
});

$('#cv-upload').change(function () {
    var filename = $('#cv-upload').val();
    filename = filename.split("\\").pop();
    filename = filename.substr(filename.lastIndexOf('\\') + 1);

    $('.file-upload label#cv-filename-display').css('visibility','visible').text(filename);
});

$('#cv-upload-submit').click(function () {
    $('#cv-upload-btn').attr("disabled", "disabled");
    $('#cv-upload-submit').attr("disabled", "disabled");
    $('#cv-upload-submit').html("Uploading...");
    var guid = generateUUID();
    var data = new FormData();
    var files = $("#cv-upload").get(0).files;
    if (files.length > 0) {
        data.append("UploadedImage", files[0]);
        data.append("FileName", guid);
    }
    $.ajax({
        url: location.protocol + "//" + location.host + '/umbraco/api/careers/fileupload',
        data: data,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function (data) {
            $('#cv-upload-btn').removeAttr("disabled");
            $('#cv-upload-submit').removeAttr("disabled");
            $('#cv-upload-submit').html("Upload");
            $('#cv-notif').text(data);
            var filename = $('#cv-upload').val();
            $('#cv-filename').val(guid + "." + filename.split(".").pop());
        },
        error: function (data) {
            $('#cv-upload-btn').removeAttr("disabled");
            $('#cv-upload-submit').removeAttr("disabled");
            $('#cv-upload-submit').html("Upload");
            $('#cv-notif').text("Fail to upload file. Please upload a file or check file size");
        }
    });

    return false;
});

function generateUUID() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
   s4() + '-' + s4() + s4() + s4();
};

$('#careers-form-submit').click(function () {
    $.post(location.protocol + "//" + location.host + '/umbraco/api/careers/sendinfo', { "": $('#careers-form').serialize() })
    .success(function (message) {
        var data = message.split("|");
        $('#careers-form-submit').text("Submit").removeAttr("disabled");
        $('.careers-form-notif').show().text(data[1]);
        if (data[0] !== "0") {
            $("#careers-form").hide();
            $('.careers-form-notif').hide();
            $('#cv-success-notif').show().text(data[1]);
        }
    })
    .error(function (message) {
        $('.careers-form-notif').show().text(message);
    });

    $('#careers-form-submit').text("Processing your request").attr("disabled", "disabled");
    return false;
});

$(window).load(function () {
    setTimeout(function () {
        $('body').removeClass('loading').removeClass('no-scroll');
    }, 500);

});

$(window).resize(function() {
    var minWidth = 767;
    if($("body").width() < minWidth) {
        $(".carousel").hover(function() { 
            $(this).attr({ 
                "title" : "Swipe left or right"
            });
        });
        $(".carousel").on("touchstart", function(event){
            var xClick = event.originalEvent.touches[0].pageX;
            $(this).one("touchmove", function(event){
                var xMove = event.originalEvent.touches[0].pageX;
                if( Math.floor(xClick - xMove) < 5 ){
                    $(this).carousel('prev');
                }
                else if( Math.floor(xClick - xMove) > -5 ){
                    $(this).carousel('next');
                }
            });
            $(".carousel").on("touchend", function(){
                    $(this).off("touchmove");
            });
        });
    } 
    var minWidth2 = 991;
    var minWidth2b = 480;
    if($("body").width() < minWidth2) {
        var anchorHTML = "<a href='/cloudemployeereviews/' style='color: #fff;'>SEE MORE</a>";
        $(".ce-resources-pricing-p").find("p").each(function(){    
            var txt = $(this).text();
            if (txt.length > 150) {
                $(this).html(txt.substring(0,150) + ".....&nbsp;");
                $(this).append(anchorHTML);
            }
        });
    }
    if($("body").width() < minWidth2b) {
        var anchorHTML = "<a href='/cloudemployeereviews/' style='color: #fff;'>SEE MORE</a>";
        $(".ce-resources-pricing-p").find("p").each(function(){    
            var txtb = $(this).text();
            if (txtb.length > 150) {
                $(this).html(txtb.substring(0,110) + ".....&nbsp;");
                $(this).append(anchorHTML);
            }
        });
    }  
    var minWidth3 = 992;
    if($("body").width() > minWidth3) {
        $(".ce-resources-pricing-p").find("p").each(function(){    
            var txt2 = $(this).text();
            if (txt2.length < 149) {
                $(this).css("text-align", "center");
            } else {
                $(this).css("text-align", "left");
            }
        });
    } 
});