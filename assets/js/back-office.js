﻿$(document).ready(function () {
    var isAnimating = false;

    toggleToMenu();
    $(window).scroll(function (event) {
        toggleToMenu();
    });

    //resizeBanner();
    $(window).resize(function () {
        var scroll = $(window).scrollTop();
        if (scroll < 1) {
            //resizeBanner();
        }
    });

    function resizeBanner() {
        var width = $(window).width();
        if (width < 1250) {
            var padding = 60;
            if ($("body").hasClass("homepage") === true) {
                padding = 195;
                $(".back-office-banner").attr("style", "z-index:1");
            } else {
                $(".back-office-banner").attr("style", "z-index:999998");
            }
            animate(".banner-padding", padding);
        } else {
            $(".back-office-banner").attr("style", "z-index:999999999");
            animate(".banner-padding", 0);
        }
    }

    function toggleToMenu() {
        var scroll = $(window).scrollTop();
        if (scroll > 0) {
            $(".back-office-banner").attr("style", "z-index:999998");
            animate(".banner-padding", 45);
        } else {
            //$(".back-office-banner").attr("style", "z-index:999999999");
            $(".back-office-banner").attr("style", "z-index:999998");
            animate(".banner-padding", 25);
            //resizeBanner();
        }
    }

    function animate(element, height) {
        if (isAnimating === false) {
            isAnimating = true;
            var currentheight = $(element).height();
            var interVal = setInterval(frame, 5);
            function frame() {
                if (currentheight === height) {
                    clearInterval(interVal);
                    isAnimating = false;
                }
                if (currentheight < height) {
                    currentheight = currentheight + 1;
                    $(".banner-padding").height(currentheight);
                }
                if (currentheight > height) {
                    currentheight = currentheight - 1;
                    $(".banner-padding").height(currentheight);
                }
            }
        }
    }
});