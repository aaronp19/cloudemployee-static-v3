$(function () {
    var body = $("body");
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 10) {
            body.addClass("scrolled");
        } else {
            body.removeClass("scrolled");
        }
    });


    var loopTestimonial = false;

    if ($('.slider ul li').length > 1) {
        loopTestimonial = true;
    }

    var testimonials = new Swiper('.slider', {
        slidesPerView: 1,
        loop: loopTestimonial,
        nextButton: '.slick-next',
        prevButton: '.slick-prev'
    });



    /* Footer images start */
    var footerImages3 = new Swiper('.footer-flip-slider-3', {
        slidesPerView: 1,
        grabCursor: true,
        effect: 'flip',
        loop: true

    });
    var footerImages2 = new Swiper('.footer-flip-slider-2', {
        slidesPerView: 1,
        grabCursor: true,
        effect: 'flip',
        loop: true,
        onSlideChangeEnd: function (swiper) {

            setTimeout(function () {
                footerImages3.slideNext();
            }, 500); //delays next call for .5 seconds
        }
    });


    var footerImages = new Swiper('.footer-flip-slider', {
        slidesPerView: 1,
        grabCursor: true,
        effect: 'flip',
        loop: true,
        autoplay: 3000,
        autoplayDisableOnInteraction: false,
        onSlideChangeEnd: function (swiper) {

            setTimeout(function () {
                footerImages2.slideNext();
            }, 500); //delays next call for .5 seconds
        }
    });
    /* Footer images end */



    $(window).scroll(function (event) {
        $(".full-width").each(function () {
            if ($(this).hasClass('.full-beyond-offshoring')) {
                if ($(this).position().top <= $(document).scrollTop() + (($(window).height() / 5) * 1)) {
                    $(this).addClass('screen-active');
                }
            } else {
                if ($(this).position().top <= $(document).scrollTop() + (($(window).height() / 5) * 3)) {
                    $(this).addClass('screen-active');
                }
            }
        });


        $('.faqs-accordion > .ac-item').each(function () {
            if ($(this).position().top <= $(document).scrollTop()) {
                $('.faq-navigation').find('li').removeClass('active');

                $('.faq-navigation').find('a[data-target=#' + $(this).attr('id') + ']').closest('li').addClass('active');

                console.log($(this).attr('id'));
            }
        });
    });


    $(document).on('click', '.faq-navigation a', function () {
        $(this).closest('ul').find('li').removeClass('active');
        $(this).closest('li').addClass('active');

        var temp = $('.faq-accordion').find($(this).data('target')).offset().top;
        $('html, body').animate({
            scrollTop: temp - 100
        }, 500);
        return false;
    });


    $('.btn-more,.get-started').click(function () {
        var e = $(this).closest('.full-width').next('.full-width');
        var temp = 0;
        if (e.find('h2').length > 0) {
            temp = e.find('h2').offset().top;
        } else {
            e = e.next(".full-width");
            temp = e.find('h2').offset().top;
        }

        if ($(this).hasClass('.get-started')) {
            temp = temp + 400;
        }

        console.log(temp);

        $('html, body').animate({
            scrollTop: temp - 150
        }, 500);
        return false;
    });




    $(document).on('click', '.office-item .overlay,.office-item .overlay span', function () {
        $(this).closest('.our-offices').find('.office-item').removeClass('active');

        $(this).closest('.office-item').addClass('active');

        if ($(window).width() <= 991) {
            $('body').addClass('no-scroll');
        }
        return false;

    });
    $(document).on('click', '.office-item .close', function () {
        $(this).closest('.office-item').removeClass('active');
        $('body').removeClass('no-scroll');

        return false;

    });

    function getVideoHeight(url, fnCallback) {

        var video = document.createElement("video");
        video.autoplay = true;
        video.oncanplay = function () {
            fnCallback(this.offsetWidth, this.offsetHeight);
            this.src = "about:blank";
            document.body.removeChild(video);
        };

        document.body.appendChild(video);
        video.src = url;

    }



    if ($('body').hasClass('homepage') || $('body').hasClass('ourstory') || $('body').hasClass('howitworks')) {
        getVideoHeight(
            "assets/vid/ce-video.mp4",
            function (w, h) {
                var conW = $('video').closest('.container').width();

                var per = (conW / w);



                $('.video-js').css({
                    width: (w * per) + 'px',
                    height: (h * per) + 'px'
                });

            }
        );
    }


    $(window).resize(function () {
        if ($('body').hasClass('homepage') || $('body').hasClass('ourstory') || $('body').hasClass('howitworks')) {


            getVideoHeight(
                "assets/vid/ce-video.mp4",
                function (w, h) {
                    var conW = $('video').closest('.container').width();

                    var per = (conW / w);



                    $('.video-js').css({
                        width: (w * per) + 'px',
                        height: (h * per) + 'px'
                    });

                }
            );
        }
    });



    $(".doaccordion").accordion(
        {
            active: false,
            collapsible: true,
            icons: false,


            beforeActivate: function (event, ui) {
                // The accordion believes a panel is being opened
                if (ui.newHeader[0]) {
                    var currHeader = ui.newHeader;
                    var currContent = currHeader.next('.ui-accordion-content');
                    // The accordion believes a panel is being closed
                } else {
                    var currHeader = ui.oldHeader;
                    var currContent = currHeader.next('.ui-accordion-content');
                }
                // Since we've changed the default behavior, this detects the actual status
                var isPanelSelected = currHeader.attr('aria-selected') == 'true';

                // Toggle the panel's header
                currHeader.toggleClass('ui-corner-all', isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top', !isPanelSelected).attr('aria-selected', ((!isPanelSelected).toString()));

                // Toggle the panel's icon
                currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e', isPanelSelected).toggleClass('ui-icon-triangle-1-s', !isPanelSelected);

                // Toggle the panel's content
                currContent.toggleClass('accordion-content-active', !isPanelSelected)
                if (isPanelSelected) { currContent.slideUp(); } else { currContent.slideDown(); }

                return false; // Cancel the default action

            }

        }
    );

    $('#contact-form-nav-submit').click(function () {
        $.post('umbraco/api/contact/sendcontact', { "": $('#contact-form-nav').serialize() })
        .success(function (message) {
            var data = message.split("|");
            if (data[0] !== "0") {
                $(".contact-form-control").hide();
            }
            $('#contact-form-nav-submit').text("Submit").removeAttr("disabled");
            $('.contact-form-notif').show().text(data[1]);
        })
        .error(function (message) {
            $(".contact-form-control").show();
        });

        $('#contact-form-nav-submit').text("Processing your request").attr("disabled", "disabled");
        return false;
    });

    $('#contact-form-modal-submit').click(function () {
        $.post('umbraco/api/contact/sendcontact', { "": $('#contact-form-modal').serialize() })
        .success(function (message) {
            var data = message.split("|");
            if (data[0] !== "0") {
                $(".contact-form-control").hide();
            }
            $('#contact-form-modal-submit').text("Submit").removeAttr("disabled");
            $('.contact-form-notif').show().text(data[1]);
        })
        .error(function (message) {
            $(".contact-form-control").show();
        });

        $('#contact-form-modal-submit').text("Processing your request").attr("disabled", "disabled");
        return false;
    });

    $('#contact-form-footer-submit').click(function () {
        $.post('umbraco/api/contact/sendcontact', { "": $('#contact-form-footer').serialize() })
        .success(function (message) {
            var data = message.split("|");
            if (data[0] !== "0") {
                $(".contact-form-control").hide();
            }
            $('#contact-form-footer-submit').text("Submit").removeAttr("disabled");
            $('.contact-form-notif').show().text(data[1]);
        })
        .error(function (message) {
            $(".contact-form-control").show();
        });

        $('#contact-form-footer-submit').text("Processing your request").attr("disabled", "disabled");
        return false;
    });
});



$(window).on('beforeunload', function () {
    $(window).scrollTop(0);
});