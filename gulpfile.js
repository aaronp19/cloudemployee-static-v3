'use strict';
 
var gulp        = require('gulp');
var concat        = require('gulp-concat');
var merge        = require('merge-stream');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
var plumber = require('gulp-plumber');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');

// Static Server + watching scss/html files
gulp.task('serve', ['sass'], function() {

	browserSync.init({
		server: "."
	});

	gulp.watch([
			   "assets/scss/*.scss"
			   ], ['sass']);
	gulp.watch("*.html").on('change', browserSync.reload);
});

gulp.task('sass', function() {
	return gulp.src([
			   		"assets/scss/jva.style.scss"
				   ])
        .pipe(sourcemaps.init())
		.pipe(concat('jva.style.css'))
		.pipe(plumber({
			handleError: function(err) {
				console.log(err);
				this.emit('end');
			}
		}))
		.pipe(sass({outputStyle : 'compressed'}))
        .pipe(sourcemaps.write('./'))
		.pipe(gulp.dest("./assets/css"))
		.pipe(browserSync.stream());
});

gulp.task('default', ['serve']);